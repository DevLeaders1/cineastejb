package com.pidev.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: BoxOffice
 * 
 */
@Entity
public class BoxOffice implements Serializable {

	private int id;
	private String description ;
	private List<Movies> movies;

	private static final long serialVersionUID = 1L;

	public BoxOffice() {
		super();
	}

	@Override
	public String toString() {
		return "BoxOffice [id=" + id + ", description=" + description + "]";
	}

	public BoxOffice(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(mappedBy = "boxoffice", fetch = FetchType.EAGER)
	public List<Movies> getMovies() {
		if(movies == null)
			movies = new ArrayList<Movies>();
		return movies;
	}

	public void setMovies(List<Movies> movies) {
		this.movies = movies;
	}



}
