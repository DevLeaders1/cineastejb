package com.pidev.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: movieCrew
 *
 */
@Entity

public class movieCrew implements Serializable {

	
	private int id;
	private String name;
	private List<Membres> Membres;
	private static final long serialVersionUID = 1L;

	public movieCrew() {
		super();
	}   
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	} 
	
	public movieCrew(String name, List<com.pidev.persistence.Membres> membres) {
		super();
		this.name = name;
		Membres = membres;
	}
	public movieCrew(movieCrew mC) {
		// TODO Auto-generated constructor stub
	}
	@ManyToMany(mappedBy="movieCrew")
	public List<Membres> getMembres() {
		return Membres;
	}
	public void setMembres(List<Membres> membres) {
		Membres = membres;
	}
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return  name ;
	}
   
}
