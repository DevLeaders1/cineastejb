package com.pidev.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Membres
 *
 */
@Entity

public class Membres implements Serializable {

	
	private int id;
	private String firstName;
	private String lastName;
	private String role;
	private List<movieCrew> movieCrew;
	private static final long serialVersionUID = 1L;

	
	
	public Membres(String firstName, String lastName, String role,
			List<com.pidev.persistence.movieCrew> movieCrew) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.role = role;
		this.movieCrew = movieCrew;
	}

	

	

	public Membres() {
		super();
	}





	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	@ManyToMany
	public List<movieCrew> getMovieCrew() {
		return movieCrew;
	}
	public void setMovieCrew(List<movieCrew> movieCrew) {
		this.movieCrew = movieCrew;
	}
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}   
	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "Membres [firstName=" + firstName + ", lastName=" + lastName
				+ ", role=" + role + ", movieCrew=" + movieCrew + "]";
	}
	
   
}
