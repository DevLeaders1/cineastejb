package com.pidev.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;



/**
 * Entity implementation class for Entity: Forum
 *
 */
@Entity

public class Forum implements Serializable {

	
	private int id;
	private String name;
	private List<Subject> subjects;
	private static final long serialVersionUID = 1L;

	public Forum() {
	}   
	
	
	/**
	 * @param name
	 */
	public Forum(String name) {
		this.name = name;
	}


	@Id    
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}
  
	
	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@OneToMany(mappedBy = "forum" , fetch = FetchType.EAGER)
	public List<Subject> getSubject() {
		if(subjects == null)
			subjects = new ArrayList<Subject>();
		return subjects;
	}
	public void setSubject(List<Subject> subject) {
		this.subjects = subject;
	}

	// g�re les deux bout de l'association
		public void addSubject(Subject subject){
			this.getSubject().add(subject);
			subject.setForum(this);
		}


		@Override
		public String toString() {
			return "Forum [id=" + id + ", name=" + name + "]";
		}
   
		
}
