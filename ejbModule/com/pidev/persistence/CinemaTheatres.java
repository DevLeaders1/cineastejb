package com.pidev.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * Entity implementation class for Entity: CinemaTheatres
 *
 */
@Entity

public class CinemaTheatres implements Serializable {

	
	private int id;
	private String name;
	private String adress;
	private int placeNumber;
	private Date date;
	private Movies movie;
	private static final long serialVersionUID = 1L;

	public CinemaTheatres() {}
	
	
	@Id  
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAdress() {
		return this.adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}   
	public int getPlaceNumber() {
		return this.placeNumber;
	}

	public void setPlaceNumber(int placeNumber) {
		this.placeNumber = placeNumber;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Lob
	public Movies getMovie() {
		return movie;
	}
	public void setMovie(Movies movie) {
		this.movie = movie;
	}
	public String toString() {
		return "CinemaTheatres [id=" + id + ", name=" + name + ", adress="
				+ adress + ", placeNumber=" + placeNumber + ", date=" + date
				+ "]";
	}


   
}
