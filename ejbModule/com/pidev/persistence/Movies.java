package com.pidev.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.lang.String;
import java.util.Arrays;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Movies
 *Master
 */
@Entity

public class Movies implements Serializable {

	
	private int id;
	private String title;
	private Date dateOfRealise;
	private String genre;
	private byte[] photo;
	private String trailer;
	private double budget;
	private String synopsis;
	private String theme;	
	private int signalisation;
	private int nb;
	private Users owner;
	private static final long serialVersionUID = 1L;
	private BoxOffice boxoffice;

	public Movies() {
		this.nb=0;
		this.signalisation=0;
		
	}   
	
	public Movies(String title, Date dateOfRealise, String genre,
			double budget, String synopsis, String theme, Users owner) {
		super();
		this.title = title;
		this.dateOfRealise = dateOfRealise;
		this.genre = genre;
		this.budget = budget;
		this.synopsis = synopsis;
		this.theme = theme;
		this.owner = owner;
		this.nb=0;
		this.signalisation=0;
		
	}

	@Id  
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	public Date getDateOfRealise() {
		return this.dateOfRealise;
	}

	public void setDateOfRealise(Date dateOfRealise) {
		this.dateOfRealise = dateOfRealise;
	}   
	public String getGenre() {
		return this.genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}   
	@Lob
	@Column(name ="photo")
	public byte[] getPhoto() {
		return this.photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}   
	public String getTrailer() {
		return this.trailer;
	}

	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}   
	public double getBudget() {
		return this.budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}   
	public String getSynopsis() {
		return this.synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}   
	public String getTheme() {
		return this.theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}   
  
	public int getSignalisation() {
		return this.signalisation;
	}

	public void setSignalisation(int signalisation) {
		this.signalisation = signalisation;
	}
	@ManyToOne( )
	public Users getOwner() {
		return owner;
	}
	public void setOwner(Users owner) {
		this.owner = owner;
	}

	public int getNb() {
		return nb;
	}
	public void setNb(int nb) {
		this.nb = nb;
	}
	public String toString() {
		return "Movies [id=" + id + ", title=" + title + ", dateOfRealise="
				+ dateOfRealise + ", genre=" + genre + ", photo="
				+ Arrays.toString(photo) + ", trailer=" + trailer + ", budget="
				+ budget + ", synopsis=" + synopsis + ", theme=" + theme
				+ ", like="+nb+ ", signalisation=" + signalisation
				+ ", owner=" + owner + "]";
	}
	
   
public void addPicture(String path) {
		
		File file = new File(path);
        byte[] bFile = new byte[(int) file.length()];
 
        try {
	     FileInputStream fileInputStream = new FileInputStream(file);
	     //convert file into array of bytes
	     fileInputStream.read(bFile);
	    
	        fileInputStream.close();
			
        } catch (Exception e) {
	     e.printStackTrace();
        }
		this.photo=bFile;
	    
			}

@ManyToOne
public BoxOffice getBoxoffice() {
	return boxoffice;
}

public void setBoxoffice(BoxOffice boxoffice) {
	this.boxoffice = boxoffice;
}
}//class
