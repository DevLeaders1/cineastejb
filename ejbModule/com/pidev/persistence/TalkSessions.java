package com.pidev.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: TalkSessions
 *
 */
@Entity
//@Table(name="t_TalkSessions")

public class TalkSessions implements Serializable {

	
	private int id;
	private String title;
	private Date begin;
	private Date end;
	private Premieres premiere;
	
	private static final long serialVersionUID = 1L;

	public TalkSessions() {
	}   
	
	
	
	public TalkSessions(String title, Date begin, Date end, Premieres premiere) {
		super();
		this.title = title;
		this.begin = begin;
		this.end = end;
		this.premiere = premiere;
	}











	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}




	public Date getBegin() {
		return begin;
	}





	public void setBegin(Date begin) {
		this.begin = begin;
	}





	public Date getEnd() {
		return end;
	}





	public void setEnd(Date end) {
		this.end = end;
	}





	





	@OneToOne
	public Premieres getPremiere() {
		return premiere;
	}





	public void setPremiere(Premieres premiere) {
		this.premiere = premiere;
	}





	@Override
	public String toString() {
		return "TalkSessions [id=" + id + ", title=" + title + ",begin=" + begin + ",end=" + end + "]";
	}
	
	
	
   
}
