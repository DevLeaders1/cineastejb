package com.pidev.persistence;


import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Entity implementation class for Entity: Users
 * SLAVE
 */
@Entity
@XmlRootElement(name="users")
public class Users  implements Serializable {

	
	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String phone;
	private String address;
	private Date dateBirth;
	private Date dateOfInscription;
	private String country;
	private String role;
	private byte[]picture;
	private List<Movies>myMovies;
	private List<Premieres>premieres;

	
	

	private static final long serialVersionUID = 1L;

	public Users() {
		this.myMovies=new ArrayList<>();
		
	}   
	public Users(String firstName, String lastName, String email,
			String password, String phone, String address, Date dateBirth) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.address = address;
		this.dateBirth = dateBirth;
		this.myMovies=new ArrayList<>();
		
		
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}   
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}   
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}   
	

	
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	} 
	@Temporal(TemporalType.DATE)
	public Date getDateBirth() {
		return this.dateBirth;
	}

	public void setDateBirth(Date dateBirth) {
		this.dateBirth = dateBirth;
	}   
	@Lob
	@Column(name ="picture")
	public byte[]getPicture() {
		return this.picture;
	}
	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public void addPicture(String path) {
		
		File file = new File(path);
        byte[] bFile = new byte[(int) file.length()];
 
        try {
	     FileInputStream fileInputStream = new FileInputStream(file);
	     //convert file into array of bytes
	     fileInputStream.read(bFile);
	    
	        fileInputStream.close();
			
        } catch (Exception e) {
	     e.printStackTrace();
        }
		this.picture=bFile;
	    
			}
	@JsonIgnore
	@OneToMany(mappedBy="owner" ,cascade=CascadeType.ALL,orphanRemoval=true )	
	public List<Movies> getMyMovies() {
		return myMovies;
	}
	public void setMyMovies(List<Movies> myMovies) {
		this.myMovies = myMovies;
	}
	
	 @Temporal(TemporalType.TIMESTAMP)
	public Date getDateOfInscription() {
		return dateOfInscription;
	}
	public void setDateOfInscription(Date dateOfInscription) {
		this.dateOfInscription = dateOfInscription;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	@JsonIgnore
	@ManyToMany
	@JoinTable( name = "Participations",
		joinColumns = {@JoinColumn( name = "User")},
		inverseJoinColumns = {@JoinColumn(name = "Premiere" )}
	)
	public List<Premieres> getPremieres() {
		return premieres;
	}
	public void setPremieres(List<Premieres> premieres) {
		this.premieres = premieres;
	}
	public String toString() {
		return "Users [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + ", password=" + password
				+ ", phone=" + phone + ", address=" + address + ", dateBirth="
				+ dateBirth + ", picture=" + Arrays.toString(picture) + "]";
	}
	
	
			
		
   
}//classEND
