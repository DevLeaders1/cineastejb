package com.pidev.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Premieres
 *
 */
@Entity
//@Table(name= "t_premieres")
public class Premieres implements Serializable {

	
	private int id;
	private String title;
	private Date date;
	private Movies movie;
	private TalkSessions talksession;
	private List <Users> user;
	
	
	private static final long serialVersionUID = 1L;

	public Premieres() {
		
	}   
	
	
	
	
	
	
	public Premieres( String title, Date date,  Movies movie) {
		
		this.title = title;
		this.date = date;
		this.movie= movie;
	}






	@Id    
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}   

/*
	//@OneToOne
	public Movies getMovie() {
		return movie;
	}


	public void setMovie(Movies movie) {
		this.movie = movie;
	}


	//@ManyToMany(mappedBy="Users")
	public List <Users> getUser() {
		return user;
	}






	public void setUser(List<Users> user) {
		this.user = user;
	}






	public TalkSessions getTalksession() {
		return talksession;
	}






	public void setTalksession(TalkSessions talksession) {
		this.talksession = talksession;
	}


*/

	
	
	@OneToOne
	public Movies getMovie() {
		return movie;
	}






	public void setMovie(Movies movie) {
		this.movie = movie;
	}





	@OneToOne(mappedBy = "premiere")
	public TalkSessions getTalksession() {
		return talksession;
	}






	public void setTalksession(TalkSessions talksession) {
		this.talksession = talksession;
	}





	@ManyToMany(mappedBy="premieres")
	public List <Users> getUser() {
		return user;
	}






	public void setUser(List <Users> user) {
		this.user = user;
	}






	@Override
	public String toString() {
		return "Premieres [id=" + id + ", date=" + date + ", title=" + title
				+ /* "movie=" + getMovie() +  */"]";
	}
   
	
	
	
	
	
}
