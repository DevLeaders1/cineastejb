package com.pidev.persistence;

import javax.persistence.EmbeddedId;
import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;



/**
 * Entity implementation class for Entity: Message
 *
 */

@Entity

public class Message implements Serializable {

	
	private Date date;
	private String text;
	private int likes;
	private int signalisation;
	
	private MessagePK pk; 
	private Subject subject;
	private Users user;
	
	
	private static final long serialVersionUID = 1L;

	public Message() {
	}   
	
	public Message(Date date, String text, int likes, int signalisation,
			 Subject subject, Users user) {

		this.date = date;
		this.text = text;
		this.likes = likes;
		this.signalisation = signalisation;
		
		this.getPk().setUserId(user.getId());
		this.getPk().setSubjectId(subject.getIdSubject());
		this.subject = subject;
		this.user = user;
	}
	
	public Message( Users user,Subject subject, String text) {
		this.text= text ;
		this.getPk().setUserId(user.getId());
		this.getPk().setSubjectId(subject.getIdSubject());
		this.subject = subject;
		this.user = user;
	}
 
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}   
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public int getSignalisation() {
		return signalisation;
	}
	public void setSignalisation(int signalisation) {
		this.signalisation = signalisation;
	}

	@EmbeddedId
	public MessagePK getPk() {
		if(pk == null)
			pk = new MessagePK();
		return pk;
	}

	public void setPk(MessagePK pk) {
		this.pk = pk;
	}

	@ManyToOne
	@JoinColumn( name = "subjectId",referencedColumnName= "idSubject", insertable = false, updatable = false)
	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	@ManyToOne
	@JoinColumn( name = "userId",referencedColumnName= "id", insertable = false, updatable = false)
	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Message [date=" + date + ", text=" + text + ", likes=" + likes
				+ ", signalisation=" + signalisation + ", pk=" + pk
				+ ", subject=" + subject + ", user=" + user + "]";
	}

   
}
