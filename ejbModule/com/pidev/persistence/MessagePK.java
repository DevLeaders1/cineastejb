package com.pidev.persistence;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MessagePK
 *
 */

@Embeddable
public class MessagePK implements Serializable {

	
	private int userId;
	private int subjectId;
	

	public MessagePK() {
	}   
	
	//@Column( name = "user_fk")
	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}   
	
	//@Column( name = "subject_fk")
	public int getSubjectId() {
		return this.subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + subjectId;
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MessagePK other = (MessagePK) obj;
		if (subjectId != other.subjectId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
   
	
}
