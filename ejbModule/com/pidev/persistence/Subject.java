package com.pidev.persistence;

import java.io.Serializable;
import java.lang.String;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;



/**
 * Entity implementation class for Entity: Subject
 *
 */
@Entity

public class Subject implements Serializable {

	
	private int idSubject;
	private String title;
	private Date date;
	private String time;
	private String description;
	private Forum forum;
	private int signalisation;
	
	

	private List<Message> messages ;
	
	private static final long serialVersionUID = 1L;


	public Subject(){}
	
	/**
	 * @param id
	 * @param title
	 * @param date
	 * @param description
	 * @param signalisation
	 */
	public Subject(String title,String description) {
		this.title = title;
		this.date = new Date();
		this.time = "12:15:06";
		this.description = description;
	
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getIdSubject() {
		return idSubject;
	}

	public void setIdSubject(int idSubject) {
		this.idSubject = idSubject;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   

	
	@ManyToOne 
	public Forum getForum() {
		return forum;
	}
	
	public void setForum(Forum forum) {
		this.forum = forum;
	}

	@OneToMany( mappedBy = "subject", cascade = CascadeType.REMOVE)
	public List<Message> getMessages() {
		if(messages == null)
			messages = new ArrayList<Message>();
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	@Override
	public String toString() {
		return "Subject [id=" + idSubject + ", title=" + title + ", date=" + date
				+ ", description=" + description + "]";
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	public int getSignalisation() {
		return signalisation;
	}

	public void setSignalisation(int signalisation) {
		this.signalisation = signalisation;
	}
   
}
