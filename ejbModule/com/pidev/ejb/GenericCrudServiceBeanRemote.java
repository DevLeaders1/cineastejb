package com.pidev.ejb;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import com.pidev.persistence.Users;

import com.pidev.persistence.Message;
import com.pidev.persistence.Movies;
import com.pidev.persistence.Subject;
import com.pidev.persistence.Users;

@Remote
public interface GenericCrudServiceBeanRemote {

	<T> T create(T t);

	<T> T find(Class<T> type, Object id);
	
	

	<T> void delete(T t);

	<T> T update(T t);

	List findWithNamedQuery(String queryName);

	List findWithNamedQuery(String queryName, int resultLimit);

	List findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters);

	List findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters, int resultLimit);

	<T> List<T> findWithNativeQuery(String sql, Class<T> type);
	
	void assignSubjectToUser(Users user, Subject subject , String text);
	
	public String JbossUrl();
	
	public Movies findMoviesByName(String name);
 
}
