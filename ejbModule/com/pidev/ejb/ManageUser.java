package com.pidev.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import com.pidev.persistence.Users;
import com.sun.security.ntlm.Client;

/**
 * Session Bean implementation class ManageUser
 */
@Stateless
public class ManageUser implements  ManageUserLocal {

	@PersistenceContext
	EntityManager em;
    /**
     * Default constructor. 
     */
    public ManageUser() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public Users authenification(String login, String password) {
		Users found=new Users();
		String jpql = "select c from Users c where c.email=:p1 and c.password=:p2";
		Query query = em.createQuery(jpql);
		query.setParameter("p1", login);
		query.setParameter("p2", password);
		try{
			found = (Users) query.getSingleResult();
			
		}catch(Exception e){
		}
		
		if (found==null)
			return null;
			else
				return found;
		
	}

	
	public void addUser(Users Users) {
		 em.persist(Users);
	      
		
	}

	
	public void updateUser(Users Users) {
		em.merge(Users);
	      
		
	}

	
	

}
