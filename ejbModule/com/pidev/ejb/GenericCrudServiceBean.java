package com.pidev.ejb;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.pidev.persistence.Message;
import com.pidev.persistence.Movies;
import com.pidev.persistence.Subject;
import com.pidev.persistence.Users;

@Stateless
@Remote(GenericCrudServiceBeanRemote.class)
@LocalBean

public class GenericCrudServiceBean implements GenericCrudServiceBeanRemote,GenericCrudServiceBeanLocal {
    @PersistenceContext
    private EntityManager em;

    @Override
    public <T> T create(T t) {
        em.persist(t);
        return t;
    }

    @Override
    public <T> T find(Class<T> type, Object id) {
        return em.find(type, id);
    }
    
   

    @Override
    public <T> void delete(T t) {
        t = em.merge(t);
        em.remove(t);
    }

    @Override
    public <T> T update(T t) {
        return em.merge(t);
    }

    @Override
    public List findWithNamedQuery(String queryName) {
        return em.createNamedQuery(queryName).getResultList();
    }

    @Override
    public List findWithNamedQuery(String queryName, int resultLimit) {
        return em.createNamedQuery(queryName).setMaxResults(resultLimit)
                .getResultList();
    }

    @Override
    public List findWithNamedQuery(String namedQueryName,
                                   Map<String, Object> parameters) {
        return findWithNamedQuery(namedQueryName, parameters, 0);          
    }

    @Override
    public List findWithNamedQuery(String namedQueryName,
                                   Map<String, Object> parameters,
                                   int resultLimit) {
        Query query = this.em.createNamedQuery(namedQueryName);
        if(resultLimit > 0) {
            query.setMaxResults(resultLimit);            
        }
        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T>  List<T> findWithNativeQuery(String sql, Class<T> type) {
        return em.createNativeQuery(sql, type).getResultList();
    }

	
	@Override
	public void assignSubjectToUser(Users user, Subject subject, String text) {
		Message message = new Message(user, subject,text);	
		em.persist(message);
		
	}
	
	public String JbossUrl(){
		String url="";
		url=new Conf().show();
		return url;
		
	}

	@Override
	public String createUser(Users user) {
		System.out.println("test");
        em.persist(user);
		return "user ajout�";
	}
	
	public Movies findMoviesByName(String name) {
		Movies found = null;
		String jpql = "select c from Movies c where c.title=:x";
		TypedQuery<Movies> query = em.createQuery(jpql, Movies.class);
		query.setParameter("x", name);
		try{
			found = query.getSingleResult();
		}catch(NoResultException e){
			Logger.getLogger(getClass().getName())
				.log(Level.INFO,"no such Movies for name="+name);
		}
		return found;
	}
	
	
	

	

	
}
