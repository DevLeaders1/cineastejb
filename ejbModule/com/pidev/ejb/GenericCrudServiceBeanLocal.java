package com.pidev.ejb;

import java.util.List;
import java.util.Map;

import com.pidev.persistence.Movies;
import com.pidev.persistence.Subject;
import com.pidev.persistence.Users;
import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

@Local 
@Path("/cineast")
public interface GenericCrudServiceBeanLocal {

	@PUT
	@Path("/createUser")
    @Consumes("application/xml")
	//@Produces("text/plain")
	public String createUser(Users user);
	

	<T> T create(T t);

	<T> T find(Class<T> type, Object id);
	
	

	<T> void delete(T t);

	<T> T update(T t);

	List findWithNamedQuery(String queryName);

	List findWithNamedQuery(String queryName, int resultLimit);

	List findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters);

	List findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters, int resultLimit);

	<T> List<T> findWithNativeQuery(String sql, Class<T> type);
	
	void assignSubjectToUser(Users user, Subject subject , String text);
	
	public String JbossUrl();
	
	public Movies findMoviesByName(String name);

}
