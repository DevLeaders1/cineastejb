package com.pidev.ejb;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.pidev.persistence.Users;

@Local
@Path("/usersAuth")
public interface ManageUserLocal {
	@GET
	@Path("/Authentificate/{login}/{password}")
	@Produces(MediaType.APPLICATION_JSON)
	public Users authenification(@PathParam("login")String login,@PathParam("password")String password);


@POST
@Path("/add")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public void addUser(Users Users);

@PUT
@Path("/update/{id}")
@Consumes(MediaType.APPLICATION_JSON)
public void updateUser(Users Users);

}