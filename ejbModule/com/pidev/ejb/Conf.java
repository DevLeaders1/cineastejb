package com.pidev.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;


@Startup
@LocalBean
public class Conf {

    public Conf() {
    }
    
    @PostConstruct
    public  String show(){
    	String url="";
    	url=System.getProperty("jboss.home.dir");
    	System.out.println(url);
    	return url;
    }

}
