package com.pidev.res;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.pidev.ejb.GenericCrudServiceBean;
import com.pidev.ejb.GenericCrudServiceBeanLocal;
import com.pidev.persistence.Movies;
import com.pidev.persistence.Users;

@Stateless
public class UsersRes  implements UsersResLocal{
	@EJB
	GenericCrudServiceBeanLocal crudFacade;

	
	public Users create(Users u) {
		
		return crudFacade.create(u);
	}

	
	public Users find( String id) {
		int pk=Integer.parseInt(id);
		return (Users) crudFacade.find(Users.class, pk);
	}

	
	public String sayHello() {
		// TODO Auto-generated method stub
		return null;
	}
	
public List<Users> getAll() {
		
		return crudFacade.findWithNativeQuery("select * from users", Users.class);
	}

	
	      

}
