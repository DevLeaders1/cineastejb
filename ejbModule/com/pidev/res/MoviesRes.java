package com.pidev.res;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.pidev.ejb.GenericCrudServiceBeanLocal;
import com.pidev.persistence.Movies;


@Stateless
public class MoviesRes implements MoviesResLocal {

	@EJB
	GenericCrudServiceBeanLocal crudFacade;
    
    public MoviesRes() {
        
    }

	public Movies create(Movies movie) {
		return crudFacade.create(movie);
	}

	public Movies find(String id) {
		int pk=Integer.parseInt(id);
		return crudFacade.find(Movies.class, pk);
	}

	
	
	public List<Movies> getAll() {
		
		return crudFacade.findWithNativeQuery("select * from movies", Movies.class);
	}

}
