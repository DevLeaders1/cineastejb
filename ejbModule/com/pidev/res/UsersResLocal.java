package com.pidev.res;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.pidev.persistence.Movies;
import com.pidev.persistence.Subject;
import com.pidev.persistence.Users;

@Local
@Path("/users")
public interface UsersResLocal {
	
	
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces( { MediaType.APPLICATION_JSON })
	public Users create(Users u);
	
	@GET
	@Path("/get")
	@Produces( { MediaType.APPLICATION_JSON})
	public Users find(@QueryParam("id") String id);
	

	@GET
	@Path("/sayHello")
	@Produces(MediaType.APPLICATION_JSON)
	public String sayHello();
	
	@GET
	@Path("/getAll")
	@Produces( { MediaType.APPLICATION_JSON })
	public List<Users> getAll();
	

}
