package com.pidev.res;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.pidev.persistence.Movies;

@Local
@Path("movies")
public interface MoviesResLocal {
	
	@POST
	@Path("/create")
	@Produces( { MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_XML)
	public Movies create(Movies movie);
	
	
	@GET
	@Path("/get")
	@Produces( { MediaType.APPLICATION_JSON })
	public Movies find(@QueryParam("id")String id);
	
	@GET
	@Path("/getAll")
	@Produces( { MediaType.APPLICATION_JSON })
	public List<Movies> getAll();

}
